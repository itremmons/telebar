SWEP.PrintName              = "Telebar"
SWEP.Version                = "0.4.6"
SWEP.Release                = "3"

SWEP.Purpose                = "Teleportation to crosshair or preset locataion"
SWEP.Instructions           = "LMB teleport to crosshair\nRMB teleport to preset/last location\n\nShift + RMB preset location\n\nAlt + RMB teleport to last/previous location\n+duck (Ctrl + Alt + RMB) required on some platforms"

SWEP.Category               = "Other"
SWEP.Spawnable              = true
SWEP.AdminSpawnable         = true

SWEP.Base                   = "weapon_base"

SWEP.ViewModel              = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel             = "models/weapons/w_crowbar.mdl"

SWEP.AutoSwitchTo           = true
SWEP.AutoSwitchFrom         = false
SWEP.Weight                 = 5
SWEP.Slot                   = 0
SWEP.SlotPos                = 2

SWEP.HoldType               = "normal"
SWEP.ViewModelFOV           = 62
SWEP.DrawCrosshair          = true
SWEP.FiresUnderwater        = true
SWEP.DrawAmmo               = false

SWEP.FireRate               = 0.2
SWEP.TeleportDelay          = 0.7
SWEP.HoldTypeDelay          = 1
SWEP.DamageTarget           = 50
SWEP.DamageTargetRadius     = 200

SWEP.SoundArrive            = "ambient/levels/labs/electric_explosion5.wav"
SWEP.EffectArrive           = "Explosion"
SWEP.EffectHeightArrive     = 0

SWEP.SoundDepart            = "ambient/levels/labs/electric_explosion3.wav"
SWEP.EffectDepart           = "cball_explode"
SWEP.EffectHeightDepart     = 40

SWEP.SoundSave              = "ambient/machines/teleport1.wav"
SWEP.EffectSave             = "GlassImpact"
SWEP.EffectHeightSave       = 0

SWEP.SoundWarn              = "ambient/machines/teleport3.wav"
SWEP.EffectWarn             = "cball_bounce"
SWEP.EffectHeightWarn       = 0

SWEP.Primary.Ammo           = "none"
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = false

SWEP.Secondary.Ammo         = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false




if not ConVarExists( "telebar_cooldown" ) then

    CreateConVar( "telebar_cooldown", SWEP.FireRate, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Cooldown Period [int/float]" )

end

if not ConVarExists( "telebar_skybox" ) then

    CreateConVar( "telebar_skybox", 1, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Target Skybox [int] 0: off, 1: on (default: 1)" )

end




if not ConVarExists( "telebar_delay" ) then

    CreateConVar( "telebar_delay", SWEP.TeleportDelay, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Teleportation Delay [int/float]" )

end

if not ConVarExists( "telebar_freeze" ) then

    CreateConVar( "telebar_freeze", 0, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Freeze player position [int] 0: off, 1: on (default: 0)" )

end

if not ConVarExists( "telebar_tracer" ) then

    CreateConVar( "telebar_tracer", 0, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Tracer [int] 0: off, 1: on (default: 0)" )

end

if not ConVarExists( "telebar_fence_radius" ) then

    CreateConVar( "telebar_fence", 1, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Departure Zone [int/float]" )

end




if not ConVarExists( "telebar_effects" ) then

    CreateConVar( "telebar_effects", 1, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "A/V Effects [int] 0: Effects off, 1: Visual and Audio Effects, 2: Sound only (default: 1)" )

end

if not ConVarExists( "telebar_sound_arrive" ) then

    CreateConVar( "telebar_sound_arrive", SWEP.SoundArrive, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Sound: Arrive [string]" )

end

if not ConVarExists( "telebar_sound_depart" ) then

    CreateConVar( "telebar_sound_depart", SWEP.SoundDepart, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Sound: Depart [string]" )

end

if not ConVarExists( "telebar_sound_save" ) then

    CreateConVar( "telebar_sound_save", SWEP.SoundSave, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Sound: Save [string]" )

end

if not ConVarExists( "telebar_sound_warn" ) then

    CreateConVar( "telebar_sound_warn", SWEP.SoundWarn, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Sound: Warn [string]" )

end




if not ConVarExists( "telebar_save" ) then

    CreateConVar( "telebar_save", 1, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Save States [int] 0: Saves off, 1: Saves Persist, 2: Saves reset on respawn (default: 1)" )

end




if not ConVarExists( "telebar_damage_target" ) then

    CreateConVar( "telebar_damage_target", SWEP.DamageTarget, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Damage Target [int/float]" )

end

if not ConVarExists( "telebar_damage_target_radius" ) then

    CreateConVar( "telebar_damage_target_radius", SWEP.DamageTargetRadius, { FCVAR_ARCHIVE, FCVAR_REPLICATED }, "Damage Target [int/float]" )

end




if CLIENT then

    SWEP.WepSelectIcon = surface.GetTextureID( "vgui/entities/telebar" )

end


if SERVER then

    savepoints = {}

    function string:split( inSplitPattern, outResults )

        if not outResults then

            outResults = {}

        end

        local theStart = 1
        local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )

        while theSplitStart do

            table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
            theStart = theSplitEnd + 1
            theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )

        end

        table.insert( outResults, string.sub( self, theStart ) )
        return outResults

    end

    function effects( effect, position, height )

        if GetConVar( "telebar_effects" ):GetInt() == 1 then

            local effectdata = EffectData()
            effectdata:SetOrigin( Vector(
                                  position[1],
                                  position[2],
                                ( position[3] + height ) ) )
            effectdata:SetMagnitude( 4 )
            effectdata:SetRadius( 4 )
            util.Effect( effect, effectdata, true, true )

        end

    end

    function sounds( effect, position )

        if GetConVar( "telebar_effects" ):GetInt() ~= 0 then

            sound.Play( effect, position, 80, 100, 1 )

        end

    end

    function holdtype( self, holdtype )

        self:SetHoldType( holdtype )
        timer.Simple( ( self.HoldTypeDelay ), function()

            if not IsValid( self ) then return end

            self:SetHoldType( self.HoldType )

        end )

    end

    function SWEP:SetupDataTables()

        self:NetworkVar( "Vector", 0, "SavePos" )
        self:NetworkVar( "Vector", 1, "PrePos" )
        self:NetworkVar( "Vector", 2, "LastPos" )

    end

    function SWEP:Deploy()

        timer.Simple( self.HoldTypeDelay , function()

            if not IsValid( self ) then return end

            self:SetHoldType( self.HoldType )

        end )

    end

    function SWEP:PrimaryAttack()
        -- if self.Owner:KeyDown( IN_SPEED ) then return end

        local trace = util.TraceLine( util.GetPlayerTrace( self.Owner ) )

        if trace.HitSky then

            if GetConVar( "telebar_skybox" ):GetInt() == 0 then return end

        end

        self.Weapon:SetNextPrimaryFire( ( CurTime() + GetConVar( "telebar_cooldown" ):GetFloat() ) )

        if GetConVar( "telebar_tracer" ):GetInt() == 1 then

            local trail = util.SpriteTrail( self.Owner, 0, Color( 0, 31, 255 ), true, 35, 1, 1, 1 / ( 15 + 1 ) * 0.5, "trails/plasma.vmt" )
	    timer.Simple( 1, function()
		trail:Remove()
            end )

        end

        effects( self.EffectWarn, trace.HitPos, self.EffectHeightArrive )
        sounds( self.SoundWarn, trace.HitPos )

        if self.Owner:GetPos() ~= self:GetSavePos() then

            self:SetLastPos( self.Owner:GetPos() )

        end

        holdtype( self, "pistol" )

        timer.Simple( GetConVar( "telebar_delay" ):GetFloat(), function()

            if not IsValid( self ) then return end

            if trace.Hit then

                if GetConVar( "telebar_damage_target" ):GetInt() > 0 then

                    util.BlastDamage(self.Owner, self.Owner, trace.HitPos, GetConVar("telebar_damage_target_radius"):GetInt(), GetConVar("telebar_damage_target"):GetInt())

                end

            end

            effects( self.EffectDepart, self.Owner:GetPos(), self.EffectHeightDepart )
            sounds( self.SoundDepart, self.Owner:GetPos() )

            self.Owner:SetLocalVelocity( Vector( 0, 0, 0 ) )
            self.Owner:SetPos( trace.HitPos )

            effects( self.EffectArrive, self.Owner:GetPos(), self.EffectHeightArrive )
            sounds( self.SoundArrive, self.Owner:GetPos() )

            self:SetPrePos( self.Owner:GetPos() )

        end )

    end

    function SWEP:SecondaryAttack()

        if GetConVar( "telebar_save" ):GetInt() == 0 then return end

        self.Weapon:SetNextSecondaryFire( ( CurTime() + GetConVar( "telebar_cooldown" ):GetFloat() ) )

        if savepoints[tostring(self.Owner)[9]] and GetConVar( "telebar_save" ):GetInt() == 1 then

            local sx = tostring( savepoints[tostring(self.Owner)[9]] ):split( "|" )[1]:split( " " )[1]
            local sy = tostring( savepoints[tostring(self.Owner)[9]] ):split( "|" )[1]:split( " " )[2]
            local sz = tostring( savepoints[tostring(self.Owner)[9]] ):split( "|" )[1]:split( " " )[3]

            self:SetSavePos( Vector( sx, sy, sz ) )

        end

        if self.Owner:KeyDown( IN_SPEED ) then

            local trace = util.TraceLine( util.GetPlayerTrace( self.Owner ) )

            if trace.HitSky then

                if GetConVar( "telebar_skybox" ):GetInt() == 0 then return end

            end

            holdtype( self, "magic" )

            self:SetSavePos( trace.HitPos )

            effects( self.EffectSave, self:GetSavePos(), self.EffectHeightSave )
            sounds( self.SoundSave, self:GetSavePos() )

        else

            -- if self.Owner:KeyDown( IN_SPEED ) then return end

            if GetConVar( "telebar_tracer" ):GetInt() == 1 then

                local trail = util.SpriteTrail( self.Owner, 0, Color( 0, 31, 255 ), true, 35, 1, 1, 1 / ( 15 + 1 ) * 0.5, "trails/plasma.vmt" )
		timer.Simple( 1, function()
		    trail:Remove()
                end )

            end

            if self.Owner:GetPos() == self:GetSavePos() or self.Owner:KeyDown( IN_WALK ) then

                if self:GetPrePos() ~= self.Owner:GetPos() then

                    if self:GetPrePos() ~= Vector( 0, 0, 0 ) then

			holdtype( self, "pistol" )

                        if GetConVar( "telebar_delay" ):GetFloat() ~= 0 then

                            effects( self.EffectWarn, self:GetPrePos(), self.EffectHeightWarn )
                            sounds( self.SoundWarn, self:GetPrePos() )

                        end

			timer.Simple( GetConVar( "telebar_delay" ):GetFloat(), function()

			    if not IsValid( self ) then return end

                            if GetConVar( "telebar_damage_target" ):GetInt() > 0 then

                                util.BlastDamage(self.Owner, self.Owner, self:GetPrePos(), GetConVar("telebar_damage_target_radius"):GetInt(), GetConVar("telebar_damage_target"):GetInt())

                            end

                            effects( self.EffectDepart, self.Owner:GetPos(), self.EffectHeightDepart)
                            sounds( self.SoundDepart, self.Owner:GetPos() )

                            self.Owner:SetLocalVelocity( Vector( 0, 0, 0 ) )
                            self.Owner:SetPos( self:GetPrePos() )

                            effects( self.EffectArrive, self.Owner:GetPos(), self.EffectHeightArrive )
                            sounds( self.SoundArrive, self.Owner:GetPos() )

                        end )

                    end

                else

                     if self:GetLastPos() ~= Vector( 0, 0, 0 ) then

                         holdtype( self, "pistol" )

                         if GetConVar( "telebar_delay" ):GetFloat() ~= 0 then

			     effects( self.EffectWarn, self:GetLastPos(), self.EffectHeightWarn )
			     sounds( self.SoundWarn, self:GetLastPos() )

			end

                        timer.Simple( GetConVar( "telebar_delay" ):GetFloat(), function()

                            if not IsValid( self ) then return end

                            if GetConVar( "telebar_damage_target" ):GetInt() > 0 then

                                util.BlastDamage(self.Owner, self.Owner, self:GetLastPos(), GetConVar("telebar_damage_target_radius"):GetInt(), GetConVar("telebar_damage_target"):GetInt())

                            end

                            effects( self.EffectDepart, self.Owner:GetPos(), self.EffectHeightDepart )
                            sounds( self.SoundDepart, self.Owner:GetPos() )

                            self.Owner:SetLocalVelocity( Vector( 0, 0, 0 ) )
                            self.Owner:SetPos( self:GetLastPos() )

                            effects( self.EffectArrive, self.Owner:GetPos(), self.EffectHeightArrive )
                            sounds( self.SoundArrive, self.Owner:GetPos() )

			end )

		   end

                end

            else

                if self:GetSavePos() ~= Vector( 0, 0, 0 ) then 

                    holdtype( self, "pistol" )

                    if GetConVar( "telebar_delay" ):GetFloat() ~= 0 then

                        effects( self.EffectWarn, self:GetSavePos(), self.EffectHeightWarn )
                        sounds( self.SoundWarn, self:GetSavePos() )

                    end

                    timer.Simple( GetConVar( "telebar_delay" ):GetFloat(), function()

                        if not IsValid( self ) then return end

                        if GetConVar( "telebar_damage_target" ):GetInt() > 0 then

                            util.BlastDamage(self.Owner, self.Owner, self:GetSavePos(), GetConVar("telebar_damage_target_radius"):GetInt(), GetConVar("telebar_damage_target"):GetInt())

                        end

                        effects( self.EffectDepart, self.Owner:GetPos(), self.EffectHeightDepart )
                        sounds( self.SoundDepart, self.Owner:GetPos() )

                        self.Owner:SetLocalVelocity( Vector( 0, 0, 0 ) )
                        self.Owner:SetPos( self:GetSavePos() )

                        effects( self.EffectArrive, self.Owner:GetPos(), self.EffectHeightArrive )
                        sounds( self.SoundArrive, self.Owner:GetPos() )

                    end )

                end

            end

        end

        s = tostring( self:GetSavePos() )
        p = tostring( self:GetPrePos() )
        l = tostring( self:GetLastPos() )
        savepoints[tostring( self.Owner )[9]] = s.."|"..p.."|"..l

    end

end

if game.SinglePlayer() then

    local function TheMenu( Panel )

        Panel:ClearControls()

        Panel:AddControl( "Slider", {
            Label = "Cooldown Period",
            Command = "telebar_cooldown",
            Type = "float",
            Min = "0.2",
            Max = "600"
        })

        Panel:AddControl( "CheckBox", {
            Label = "Target Skybox",
            Command = "telebar_skybox",
            Type = "bool"
        })

        Panel:AddControl( "Slider", {
            Label = "Delay/Warning",
            Command = "telebar_delay",
            Type = "float",
            Min = "0",
            Max = "5"
        })

        Panel:AddControl( "CheckBox", {
            Label = "Freeze Player",
            Command = "telebar_freeze",
            Type = "bool"
        })

        Panel:AddControl( "CheckBox", {
            Label = "Tracer",
            Command = "telebar_tracer",
            Type = "bool"
        })

        Panel:AddControl( "Slider", {
            Label = "Departure Zone",
            Command = "telebar_fence_radius",
            min = "0",
	    max = "300"
        })

        Panel:AddControl( "Slider", {
            Label = "Effects",
            Command = "telebar_effects",
            Type = "int",
            Min = "0",
            Max = "2"
        })

        Panel:AddControl( "Header", {
            Text = "Mode" ,
            Description	=[[
            0: None
            1: Sound and Visual Effects
            2: Sound Only
            ]]
        })

        Panel:AddControl( "Slider", {
            Label = "Save Points",
            Command = "telebar_save",
            Type = "int",
            Min = "0",
            Max = "2"
        })

        Panel:AddControl( "Header", {
            Text = "Mode" ,
            Description	=[[
            0: None
            1: Persist
            2: Reset on respawn
            ]]

	})

        Panel:AddControl( "Slider", {
            Label = "Damage Target",
            Command = "telebar_damage_target",
            Type = "float",
            Min = "0",
            Max = "100"
        })

        Panel:AddControl( "Slider", {
            Label = "              Radius",
            Command = "telebar_damage_target_radius",
            Type = "float",
            Min = "0",
            Max = "300"
        })

    end

    local function optionsmenu()

        spawnmenu.AddToolMenuOption( "Options", "Telebar", "telebar_menu", "Settings", "", "", TheMenu )

    end
    hook.Add( "PopulateToolMenu", "telebar_menu", optionsmenu )

end

print( "Telebar: init complete ("..SWEP.Version.."-"..SWEP.Release..")" )
